package settingsreader

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// Read reads and returns wanted setting
func Read(s string) string {
	settings := make(map[string]string)

	rawSettingsJSON, err := ioutil.ReadFile("conf.json")

	if err != nil {
		fmt.Println("Error with file read: ")
		fmt.Println(err)
		panic(err)
	}

	err = json.Unmarshal(rawSettingsJSON, &settings)

	if err != nil {
		fmt.Println("Error with parsing json: ")
		fmt.Println(err)
	}

	return string(settings[s])
}
