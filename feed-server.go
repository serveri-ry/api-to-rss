package main

import (
	enfeed "api-to-rss/ENfeed"
	fifeed "api-to-rss/FIfeed"
	"api-to-rss/index"
	"api-to-rss/settingsreader"

	"github.com/labstack/echo"
)

func main() {
	e := echo.New()

	e.GET("/", index.ReturnIndex)
	e.GET("/fi", fifeed.FinnishFeed)
	e.GET("/en", enfeed.EnglishFeed)
	e.Logger.Info(e.Start(settingsreader.Read("ServerListenPort")))
}
