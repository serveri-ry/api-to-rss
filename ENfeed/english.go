package enfeed

import (
	"api-to-rss/settingsreader"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/gomarkdown/markdown"
	"github.com/labstack/echo"
)

/*type Item struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
	Content     string `xml:"content"`
}*/

type channel struct {
	Language    string   `xml:"language"`
	Title       string   `xml:"title"`
	Description string   `xml:"description"`
	Link        string   `xml:"link"`
	Atom        string   `xml:"atom"`
	Generator   string   `xml:"generator"`
	Item        []EnPost `xml:"item"`
}

type EnPost struct {
	En_title   string `xml:"title"`
	URL        string `xml:"link"`
	Guid       string `xml:"guid"`
	En_Content string `xml:"description"`
	Publish    string `xml:"pubDate"`
}

func EnglishFeed(c echo.Context) error {

	// Set basic info for RSS feed
	feedEN := &channel{
		Language:    "en",
		Title:       settingsreader.Read("feedTitleEN"),
		Description: settingsreader.Read("feedDescriptionEN"),
		Link:        settingsreader.Read("ENpublicFeedUrl"),
		Generator:   "Serveri ry RSS generator - developed by Jim.bo",
	}

	// Request fresh content from the api
	resp, err := http.Get(settingsreader.Read("apiUrl"))
	if err != nil {
		fmt.Println("api request failed")
	}
	defer resp.Body.Close()

	// Read the response body
	apiJSON, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("response reading failed")
	}

	// Create temporary slice for posts
	postSlice := make([]EnPost, 0)

	// Unmarshal JSON to the temporary slice
	json.Unmarshal(apiJSON, &postSlice)

	var fullurl string
	var mdContent []byte

	// Processing of individual items in posts
	for i, post := range postSlice {
		// Create full urls from post identifiers
		fullurl = settingsreader.Read("baseUrlForPosts") + post.URL
		// Adds base url to individual post urls
		postSlice[i].URL = fullurl
		// Set guid
		postSlice[i].Guid = fullurl

		// Convert markdown to html
		mdContent = []byte(postSlice[i].En_Content)
		postSlice[i].En_Content = string(markdown.ToHTML(mdContent, nil, nil))

		pub, _ := time.Parse("2006-01-02T15:04:05Z07:00", postSlice[i].Publish)
		postSlice[i].Publish = pub.Format("Mon, 02 Jan 2006 15:04:05 -0700")
	}

	// Set
	feedEN.Item = postSlice

	out, _ := xml.MarshalIndent(feedEN, " ", " ")

	head := "<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom'>\n"
	close := "\n</rss>"

	feed := head + string(out) + close

	atom := "<atom:link href='" + settingsreader.Read("ENpublicFeedUrl") + "' rel='self' type='application/rss+xml' />"
	feedWithAtomLink := strings.Replace(feed, "<atom></atom>", atom, 1)

	//fmt.Println(feedWithAtomLink)

	return c.XMLBlob(http.StatusOK, []byte(feedWithAtomLink))
}
