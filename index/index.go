package index

import (
	"api-to-rss/settingsreader"
	"net/http"

	"github.com/labstack/echo"
)

func ReturnIndex(c echo.Context) error {
	response := "<!DOCTYPE html><html lang='en'><head>    <meta charset='UTF-8'>    <meta name='viewport' content='width=device-width, initial-scale=1.0'>    <title>Serveri ry rss feeds</title></head><body>    <h1>RSS Feeds</h1>    <p>You can likely find this represented more accurately at <a href='https://serveriry.fi/connect'>Serveri ry connect website</a>    </p>    <p>Feed url for finnish posts is <a href='"
	response = response + settingsreader.Read("FIpublicFeedUrl") + "'>" + settingsreader.Read("FIpublicFeedUrl") + "</a></p>"
	response = response + "<p>Feed url for english posts is <a href='" + settingsreader.Read("ENpublicFeedUrl") + "'>" + settingsreader.Read("ENpublicFeedUrl") + "</a></p>"
	return c.HTML(http.StatusOK, response)
}
